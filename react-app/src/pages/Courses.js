import { Fragment } from 'react'
import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'


export default function Courses() {
  
    // console.log(coursesData);
    // console.log(coursesData[0]);

    const courses = coursesData.map(course => {
        return(
            <CourseCard key={course.id} courseProp={course} />
        )
    })

    return (
        <Fragment>
         <h1 className='text-center mt-3'>Courses</h1>
         {courses}
        </Fragment>
  )
}

