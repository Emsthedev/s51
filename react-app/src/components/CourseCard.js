import { useState } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
    //is to check if data was successfully passed
    //console.log(props);
    // Every component receives information in a form  of an object
    //console.log(typeof props);
    //console.log({courseProp});

    const { name, description, price, _id } = courseProp;

    /* 
        Use the state hook for this component to be able to store its state.
        States are used to keep track of the information related to individual
            components/elements
            Syntax:
            const [getter, setter] = useState(initialGetterValue);
    */

    //
    //  const [count, setCount] = useState(0)
    //  //console.log(useState(0));
    // 
   
    //  function enroll(){
    //     setCount(count + 1)
    //     console.log('Enrollees ' +  count);

        
    //  }

    const [count, setCount] = useState(0);
	const [seatCount, seatSetCount] = useState(30);
	console.log(useState(0));

	function enroll(){
		setCount(count + 1);
		seatSetCount(seatCount - 1)
		if (seatCount === 0 && count === 30){
			setCount (count);
			seatSetCount(seatCount);
			alert('Sorry, there are no more seats available.');
		}
		
	}

   


    return(
       
       <Row className="my-3">
        <Col xs={12} md={6} >
            <Card className="cardCourses p-3" >
                <Card.Body >
                   
                    <Card.Header className="text-center card-header">{name}</Card.Header>
                    <Card.Subtitle className="mt-4 text-">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Text>Enrollees: {count}</Card.Text>
                    <Card.Text>Seats Available: {seatCount}</Card.Text>
                    <Button className='bg-primary' onClick={enroll}>Enroll</Button>
                   
                </Card.Body>
            </Card>
        </Col>

        
       </Row>
       
    )
};